/**
 * Copyright (c) 2016 Voidware Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * contact@voidware.com
 */

/* Program to fold paragraphs in a text file
 * paragraphs are sections of text separated by a blank lines
 */

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

struct Transform
{
    std::istream&       _in;
    std::ostream&       _out;

    int                 _col;
    int                 _width;

    bool                _removeSpace;
    bool                _md;

    static const int    maxWord = 64;
    char                _word[maxWord];
    char*               _wp;

    Transform(std::istream& in, std::ostream& out) : _in(in), _out(out)
    {
        _width = 0;
        _col = 0;
        _wp = _word;
        _removeSpace = false;
        _md = false;
    }

    void  _flush(bool skipfirstspace = false)
    {
        const char* p = _word;
        if (p < _wp)
        {
            if (*p == ' ' && skipfirstspace) ++p; // skip initial space
            while (p < _wp)
            {
                _out.put(*p);
                ++_col;
                ++p;
            }
            _wp = _word;
        }
    }
    
    bool _emit(char c)
    {
        if (c == '\n')
        {
            _flush();
            _col = 0;
            _out.put('\n');
        }
        else
        {
            int col = _col + (_wp - _word);
            if (_width > 0 && col > _width)
            {
                _out.put('\n');
                _col = 0;
                _flush(true);
            }

            if (c == ' ')
            {
                if (_removeSpace &&
                    (col == 0 || (_wp != _word && _wp[-1] == ' '))) goto done;
                _flush();
                
            }

            // buffer
            if (_wp >= _word + maxWord) _flush();
            *_wp++ = c;
        }

    done:;
        return !_out.bad();
    }

    bool transform()
    {
        int c;
        bool ok = true;
        while ((c = _in.get()) != EOF && ok)
        {
            if (c == '\t')
            {
                // expand tabs into 8 spaces
                for (int i = 0; i < 8; ++i) _emit(' ');
            }
            else
            {
                // when there are two newlines, we have a blank line
                // in such cases, keep the blank line
                //
                // For only one newline, change it to a space and
                // allow the emitter to wrap when necessary.
                if (c == '\n')
                {
                    c = _in.get();
                    if (c == EOF)
                    {
                        _emit('\n');
                        break;
                    }

                    bool allow = c == '\n';
                    if (_md)
                    {
                        if (c == '*' || c == '#') allow = true;
                    }

                    if (allow)
                        _emit('\n');
                    else
                        _emit(' ');
                }
                ok = _emit(c);
            }
        }

        _flush();
        return ok;
    }
};

using namespace std;

int main(int argc, char** argv)
{
    int res = -1;
    const char* infile = 0;
    const char* outfile = 0;

    int width = 80;
    bool removeSpace = false;
    bool md = true;

    // options are -w <linelengh> eg -w 100
    // -s remove extraneous duplicate white space
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!strcmp(argv[i], "-w") && i < argc-1) width = atoi(argv[++i]);
            else if (!strcmp(argv[i], "-s")) removeSpace = true;
            else if (!strcmp(argv[i], "-nomd")) md = false;
        }
        else
        {
            if (!infile) infile = argv[i];
            else outfile = argv[i];
        }
    }

    if (!infile || (outfile && !strcmp(infile, outfile)))
    {
        cerr << "Usage " << argv[0] << "[-w <width>] [-s]  <infile> [<outfile>]\n";
        return -1;
    }

    ifstream in(infile);
    if (in.good())
    {
        ofstream outf;

        ostream* out = 0;
        if (outfile)
        {
            outf.open(outfile, ios_base::trunc);
            if (outf.good())
            {
                out = &outf;
            }
            else cerr << "Can't open '" << outfile << "'\n";
        }
        else out = &cout;
            
        if (out)
        {
            Transform t(in, *out);
            t._width = width;
            t._removeSpace = removeSpace;
            t._md = md;
            if (t.transform()) res = 0;
        }
    }
    else cerr << "Can't open '" << infile << "'\n";

    return res;
}
