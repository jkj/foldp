# FOLDP

Fold paragraphs in plain text. For example as preprocessor to Beyond Compare.

Paragraphs are sections of text separated by a blank line

## Usage

`foldp filename [-s] [-w <linelength>] [outputfile]`

* -w <linelength>
eg -w 100.
Wrap at 100

* -s 
remove extraneous white space. eg duplicates.

## Examples

```
foldp myfile.txt outfile.txt
foldp -w 80 myfile.txt outfile.txt
foldp -s -w 80 myfile.txt outfile.txt
foldp -s -w 80 myfile.txt > foo.txt
```








